/* ** Scrapper for bus.es **

    by Victoriano Izquiedo
    me@victoriano.me

  ** August of 2014 **

  Get All Regions and Towns from Spain
*/

var cheerio = require("cheerio");
var request = require("request");

/* Connection to DB */
var mongoose = require('mongoose');
var config = require('./config');
var db = require('./models');

/* MongoDB address mongodb://localhost/buses */
mongoose.connect(config.creds.mongoose_auth_local);


/* *** Set Cookies  *** */

var cookies = { JSESSIONID: '', ARRAffinity: ''};
var cookie = 'JSESSIONID='+cookies.JSESSIONID+'; ARRAffinity='+cookies.ARRAffinity

var set_cookies = function(callback){
  request(
    { uri: "http://www.fomento.es/portalsitranbus/Mapa.jsp?idioma=ES"},
    function(error, response, body) {
      cookies.JSESSIONID = response.headers["set-cookie"][1].split('=')[1].split(';')[0] ;
      cookies.ARRAffinity = response.headers["set-cookie"][0].split('=')[1].split(';')[0] ;
      cookie = 'JSESSIONID='+cookies.JSESSIONID+'; ARRAffinity='+cookies.ARRAffinity
      callback();
    });
}

/* *** Get all Regions from Spain  *** */

var get_regions_request = function(callback){
  request(
    { uri: "http://www.fomento.es/portalsitranbus/servlet/ServletController?modulo=Localidades&accion=inicio",
      encoding:'binary',
      headers: {
        'Cookie': cookie
      }
    },
    function(error, response, body) {
      var regions = [];
      var $ = cheerio.load(body);
      $('#cgprovdes option').each(function(i, element){
        regions.push({ code: $(this).val(), name: $(this).text()});
      });
      regions.shift();
      callback(regions);
    });
}

/* *** Get Towns for a Region *** */

var get_towns_request = function(region, callback){

  function req_towns(callback) {
    request({
        method: "POST",
        uri: "http://www.fomento.es/portalsitranbus/servlet/ServletController",
        encoding:'binary',
        headers: {
          'Cookie': cookie
        },
        form:{
          'modulo':'Localidades',
          'cgprovori':region.code,
          'accion#selec_provori': 'Seleccionar'
        }
      },
      function(error, response, body) {
        //Check if server is up
        if (!error && response.statusCode == 200) {

          //Check it's empty or content body
          if(response.headers['content-length'] > 0){
             //Check if it is an error page
             var $ = cheerio.load(body);
             if($('.mensajeErrorFuera').length == 1){
               console.log("error page for " + region.name);
               req_towns(callback);
             }else{
               var towns = [];
               $('#cgpoblori option').each(function(i, element){
                 var cg_twon = $(this).val();
                 towns.push({ cg_town: $(this).val() });
               });
               towns.shift();
               console.log("valid page for " + region.name + " code " + region.code + " / towns to be added: " + towns.length);
               callback(region, towns);
             }

          }else{
            console.log("empty response");
          }
        }else{
            console.log("server down");
        }

      });
    }

    return req_towns(callback);
}

/* ** Calling regions one by one to be
        requested and stored
        without concurrency ** */

var towns_to_db = function(regions){
  var i = 0;
  function t_db(i) {
    console.log("calling #: " + i);

    if(regions.length > i){
      get_towns_request(regions[i], function(region, towns){
        db.save_multiple_towns(region.code, towns, function(message){
          console.log(message);
          t_db(++i);
        });
      });
    }else{
      mongoose.disconnect();
    }
  }

  return t_db(i);
}

/* *** Main function *** */

/* Get and save all regions (52) from bus.es in the DB */
/*
set_cookies(function(){
  get_regions_request(function(regions){
    //console.log(regions);
    db.save_multiple_regions(regions, function(message){
      console.log(message);
    });
  });
});
*/

/* Get and save all towns (8228) from bus.es in the DB */

set_cookies(function(){
  get_regions_request(function(regions){
    db.get_my_regions(function(regions){
       towns_to_db(regions);
    });
  });
});
