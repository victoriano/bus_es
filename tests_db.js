/* ** Scrapper for bus.es **

    by Victoriano Izquiedo
    me@victoriano.me

  ** August of 2014 **

  Test for writing and reading from the DB
*/

/* Connection to DB */
var mongoose = require('mongoose');
var config = require('./config');
var db = require('./models');

/* MongoDB address ie mongodb://localhost/buses */
mongoose.connect(config.creds.mongoose_auth_local);


/* *** Saving *** */

//Simple save a region
/*
var region = new db.Region({code: '18', name: "Granada"});
region.save(function(err, object, numberAffected){
  console.log(object);
  mongoose.disconnect();
});
*/

//var town = new model.Town({cg_town: '071000DURCAL', name: "DURCAL", muni_de: '071', pobl_de: '0000', region_id: ''});

//Save a region with custom function
/*
db.save_region('19', 'MALAGA', function(object){
  console.log(object);
});

db.save_region('18', 'GRANADA', function(object){
  console.log(object);
});

*/

//Saving an array of regions at once
/*
var regions = [{code: '18', name: 'GRANADA'}, {code: '19', name: 'MALAGA'}]
var finished = false;
var k = 0;

for (i = 0; i < regions.length; i++) {

  db.save_region(regions[i].code, regions[i].name, function(object){
    console.log("saving");
    console.log(object);
    ++k;
    if(k == towns.length-1){ mongoose.disconnect(); }
  });

}
*/

// Save a town
/*
db.save_town('18', '0710000DURCAL', function(object){
  console.log(object);
  mongoose.disconnect();
});
*/

//Save an array of towns
/*
var towns = [
  {cg_town: '2960000ZAIDA (LA)' },
  { cg_town: '2970000ZARAGOZA'},
  ]

db.save_multiple_towns('50', towns, function(message){
  console.log(message);
});
*/

/* *** Querying *** */

// Get the name of the region of a given town
/* */
// {name: 'ALTEA'}
//{name: 'DURCAL'}
// {region_id: "53e52c918dfc3b9e709acad2"}
// {region_id: "53e52c918dfc3b9e709acac0"}
//RABANERA DEL CAMPO (CUBO DE LA SOLANA)
//{name: 'RABANERA DEL CAMPO (CUBO DE LA SOLANA)'}

db.get_region_of('ALTEA', function(region){
  console.log(region);
  mongoose.disconnect();
});


// Get all Regions stored in DB

/*db.get_my_regions(function(docs){
  console.log(docs);
});
*/

/* *** Removing *** */


//Dropping collections of the DB and removing
/*
mongoose.connection.collections['regions'].drop( function(err) {
    console.log('collection dropped');
    mongoose.disconnect();
});

*/

/*
mongoose.connection.collections['towns', 'regions'].drop( function(err) {
    console.log('collection dropped');
    mongoose.disconnect();
});

*/

/*
Region.remove({}, function(err) {
   console.log('collection removed')
});
*/
