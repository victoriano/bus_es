var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/* ** Region Model ** */
var regionsSchema = new Schema({
  code: { type: String, unique: true },
  name:  String
});

var Region = mongoose.model('Region', regionsSchema );
exports.Region = Region;

/* ** Town Model ** */
var townsSchema = new Schema({
  cg_town: { type: String, unique: true },
  name:  String,
  muni_de: String,
  pobl_de: String,
  region_id:  {
    type: String,
    ref: 'Region'
  }
});

var Town = mongoose.model('Town', townsSchema );
exports.Town = Town;


/* ** Trip Model ** */
var tripsSchema = new Schema({
  route:{
    type: String,
    ref: 'Route'
  },
  starting_stop:  {
    type: String,
    ref: 'Stop'
  },
  final_stop:  {
    type: String,
    ref: 'Stop'
  },
  date:   String,
  t_dep:  String,
  t_arr:  String,
  time:   String,
  price:  String,
  distance: String,

});

var Trip = mongoose.model('Trip', tripsSchema );
exports.Trip = Trip;

/* ** Stop Model ** */
var stopsSchema = new Schema({
  name: { type: String, unique: true },
  town:  {
    type: String,
    ref: 'Town'
  }
});

var Stop = mongoose.model('Stop', stopsSchema );
exports.Stop = Stop;

/* ** BusCompany Model ** */
var buscompaniesSchema = new Schema({
  name: { type: String, unique: true },
  url: String,
  phone: String,
  address: String,
  location: String
});

var BusCompany = mongoose.model('BusCompany', buscompaniesSchema );
exports.BusCompany = BusCompany;

/* ** Route Model ** */
var routesSchema = new Schema({
  name: { type: String, unique: true },
  origin: String,
  destiny: String,
  bus_company: {
    type: String,
    ref: 'BusCompany'
  }
});

var Route = mongoose.model('Route', routesSchema );
exports.Route = Route;


/* ** Saving models ** */


/* Saving an arrays of regions */

var save_region = function(code, name, callback){
  var region = new Region({code: code, name:name})
  region.save(function(err, object, numberAffected){
    callback(object);
  });
}

exports.save_region = save_region ;

var save_town = function(code, cg_town, callback){
  var region = Region.findOne({ 'code': code }, 'name code', function (err, region) {
  //console.log('%s is %s', region.name, region.code );

  var town = new Town({
    cg_town: cg_town,
    name: cg_town.substring(7, cg_town.lenght),
    muni_de: cg_town.substring(0, 3) ,
    pobl_de: cg_town.substring(3, 7),
    region_id: mongoose.Types.ObjectId(region.id)
    });
  town.save(function (err, object, numberAffected){
    callback(object);
  });

  })
}

exports.save_town = save_town ;

var save_multiple_regions = function(regions, callback){
  var finished = false;
  var k = 0;

  for (i = 0; i < regions.length; i++) {
    save_region(regions[i].code, regions[i].name, function(object){
      console.log("saving");
      console.log(object);
      ++k;
      if(k == regions.length-1){
        callback("all regions were saved");
        mongoose.disconnect();
        }
    });

  }
}

exports.save_multiple_regions = save_multiple_regions ;

/* Saving to DB towns from a region */

var save_multiple_towns = function(region_code, towns, callback){
  var finished = false;
  var k = 0;
  if(towns.length == 0){callback("no towns for this region");}
  for (i = 0; i <= towns.length-1; i++) {
    save_town(region_code, towns[i].cg_town, function(object){
      //check if this was the last town
      if( k == towns.length-1 ){
        callback("all towns for this region were saved");
      }else{
        ++k;
      }
    });

  }
}

exports.save_multiple_towns = save_multiple_towns ;


/* Save and upserts for BusCompany, Route, Stop and Trip */

var upsert_buscompany = function(buscompany, callback){
  var exits_company = BusCompany.find({name:buscompany.name},{'_id': 1});
  exits_company.exec(function (err, company) {
    if(company.length < 1){
      var b_company = new BusCompany(buscompany);
      b_company.save(function(err, object, numberAffected){
        callback(object);
      });
    }else{
      callback(company[0]);
    }
  });

}

exports.upsert_buscompany = upsert_buscompany ;

var upsert_route = function(route_name, route_origin, route_destiny, bus_company, callback){
  var exits_route = Route.find({name:route_name},{'_id': 1});
  exits_route.exec(function (err, route) {
    if(route.length < 1){
      var route = new Route({
        name: route_name,
        origin: route_origin,
        destiny: route_destiny,
        bus_company: bus_company.id
      });
      route.save(function(err, object, numberAffected){
        callback(object);
      });
    }else{
      callback(route[0]);
    }
  });

}

exports.upsert_route = upsert_route ;

var save_stop = function(stop, callback){
  Town.find({name:stop.town},{'_id': 1, 'name':1}).exec(function(err, town_found){
    if(town_found<1){
      console.log("No town found with: " + stop.town);
      //Case 'PUENTE DE LA CERRADA'
      //{'name': {'$regex': 'PUENTE DE LA CERRADA'}}


    }else{
      var new_stop = new Stop({
        name: stop.name,
        town: town_found[0].id,
      });
      new_stop.save(function(err, object, numberAffected){
        callback(object);
      });
    }
  });
}

var upsert_stop = function(stop, callback){
  Stop.find({name:stop.name},{'_id': 1, 'name':1}).exec(function(err, stop_found){
      if(stop_found.length < 1){
        save_stop(stop, function(object_saved){
          callback(object_saved);
        });
      }else{
        callback(stop_found);
      }
  });
}

var upsert_stops = function(starting_stop, final_stop, callback){
  var stops = [];
  upsert_stop(starting_stop, function(origin){
    upsert_stop(final_stop, function(destiny){
      stops.push(origin);
      stops.push(destiny);
      callback(stops);
    });
  });

}

exports.upsert_stops = upsert_stops ;

var save_trip = function(route, stops, itinerary, callback){
  var trip = new Trip({
    route: route.id,
    starting_stop: stops[0].id,
    final_stop: stops[1].id,
    date:   itinerary.date,
    t_dep:  itinerary.t_dep,
    t_arr:  itinerary.t_arr,
    time:   itinerary.time,
    price:  itinerary.price,
    distance: itinerary.distance
  });
  trip.save(function(err, object, numberAffected){
    callback(object);
  });
}

exports.save_trip = save_trip ;


/* ** Queries ** */

/* Retrieve an array of the codes of the regions stored in the DB */
var get_my_regions = function(callback){
  var regions = Region.find({},{'_id': 0, code:1, name:1});
  regions.exec(function (err, docs) {
    callback(docs);
  });
}

exports.get_my_regions = get_my_regions ;


/* Get towns of a given region by name */
var get_towns_for_region = function(region_name, callback){
  var region = Region.find({name: region_name});
  region.exec(function(err, region){
    Town.find({region_id: region[0]._id}, {_id:0, __v:0})
    .sort([['name', 'ascending']])
    .populate('region_id', {code: 1, _id:0})
    .exec(function (err, towns) {
      if (err) return handleError(err);
      callback(towns);
    })
  });

}

exports.get_towns_for_region = get_towns_for_region ;

/* Returns the name of the region that a town belongs to */
var get_region_of = function(town_name, callback){
  //Town.findOne({name: new RegExp('^'+town_name+'$', "i")})
  Town.findOne({name: town_name})
  .populate('region_id')
  .exec(function (err, town) {
    if (err) return handleError(err);
    console.log('%s is the region of %s', town.name, town.region_id.name);
    mongoose.disconnect();
    callback(town.region_id.name);
  })
}

exports.get_region_of = get_region_of ;
