/* ** Scrapper for bus.es **

    by Victoriano Izquiedo
    me@victoriano.me

  ** August of 2014 **
*/

var cheerio = require("cheerio");
var request = require("request");

/* Connection to DB */
var mongoose = require('mongoose');
var config = require('./config');
var db = require('./models');

/* MongoDB address mongodb://localhost/buses */
mongoose.connect(config.creds.mongoose_auth_local);


/* **** Request a trip *** */

var request_trip = function(origin, destiny, date, callback){

  function req_trip(callback) {
    request({
      method: "POST",
      uri: "http://www.fomento.es/portalsitranbus/servlet/ServletController",
      encoding:'binary',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cookie': cookie
      },

      form:{
        'modulo':'Localidades',

        'cgprovori': origin.region_id.code,
        'cgpoblori': origin.cg_town,

        'Destino': destiny.name,
        'prov_de': destiny.region_id.code,
        'muni_de': destiny.muni_de,
        'pobl_de': destiny.pobl_de,

        'dia': date.split('/')[0],
        'mes': date.split('/')[1],
        'anyo':date.split('/')[2],
        'accion#Busca_Conexiones': 'Ver+Trayectos'
      }

    }, function(error, response, body) {
      //log_response(response);
      //Check if server is up
      if (!error && response.statusCode == 200) {

        //Check it's empty or content body
        if(response.headers['content-length'] > 0){
          console.log("from: " + origin.name + " to " + destiny.name);
           callback(body);
        }else{
          console.log("empty response");
          set_cookies(function() {
            console.log("cookies renewed...");
            req_trip(callback);
          });

        }
      }else{
          console.log("server down");
      }


    });}

    return req_trip(callback);

}

/* *** Request a route *** */

var request_route = function(itinerary, callback){
  request(
    { uri: 'http://www.fomento.es'+ itinerary.link,
      encoding:'binary',
      headers: {
        'Cookie': cookie
        }
    },
    function(error, response, body) {
      parse_routes_response(itinerary, body, function(mssg){
        callback(mssg);
      });

    });
}

/* *** Utility functions  *** */

var set_cookies = function(callback){
  request(
    { uri: "http://www.fomento.es/portalsitranbus/Mapa.jsp?idioma=ES"},
    function(error, response, body) {
      cookies.JSESSIONID = response.headers["set-cookie"][1].split('=')[1].split(';')[0] ;
      cookies.ARRAffinity = response.headers["set-cookie"][0].split('=')[1].split(';')[0] ;
      cookie = 'JSESSIONID='+cookies.JSESSIONID+'; ARRAffinity='+cookies.ARRAffinity
      callback();
    });
}


var log_response = function(response){

  var now = new Date();
  now.setHours(now.getHours() + 2);
  var jsonDate = now.toJSON();

  console.log(" ");
  console.log(jsonDate);
  console.log("----> Request ");
  //console.log(response.request.headers);
  console.log("cookie envíada");
  console.log(response.request.headers.Cookie);

  console.log(" ");

  console.log("----> Response ");
  //console.log(response.headers);

}

/* *** Parsing itinerary request Response *** */

var parse_itineraries_response = function(body, callback){
  var $ = cheerio.load(body);
  if($('.resultados') <1){
    callback("no trips between these towns")
  }else{
    parse_itineraries(body, function(trips){
      callback(trips.length + " trips were added");
    });
  }
}

var parse_itineraries = function(body, callback){
  var $ = cheerio.load(body);
  console.log("// There are Trips! //");
  console.log($('#ddOrigenDestino').text());
  console.log($('#ddFecha').text());

  var itineraries = []
  $('.filaImpar, .filaPar' ).each(function(i, element){
    t_dep=  $(this).find('td').eq(0).text(),
    t_arr=  $(this).find('td').eq(1).text(),
    time = $(this).find('td').eq(2).text();
    distance = $(this).find('td').eq(3).text().split(/\s+/)[0];
    price = $(this).find('td').eq(4).text().split('€')[0].trim();
    link = $(this).find('td').eq(5).children('a').attr('href');
    itinerary = {date: date, t_dep: t_dep, t_arr: t_arr, time: time, distance: distance, price: price, link: link}
    itineraries.push(itinerary);
  });

  console.log("Itineraries to be explored: " + itineraries.length);

  request_routes(itineraries, function(c){
    callback(c);
  });

}

/* *** Parsing Trip request Response *** */

var request_routes = function(itineraries, callback){
  var i = 0;

  function r_r(i){
    if(itineraries.length > i){
      request_route(itineraries[i], function(mssg){
          console.log(mssg);
          ++i;
          r_r(i);
      })
    }else{
      callback("all routes requested");
    }
  }

  return r_r(i);
}

var parse_routes_response = function(itinerary, body, callback){
  var $ = cheerio.load(body);

  //Route Info
  var route_name = $('#ddRuta').text();
  route = parse_origin_destiny_in_route(route_name);
  route_origin = route.origin;
  route_destiny =  route.destiny;

  console.log("Route " + route_name);

  //TODO binary vs UTF8 decoding issues
  //'PZA COCA DE LA PIÑERA, Nº 6'
  /*if(route_name == "JAEN  -   ALMUÃECAR"){
    console.log(body);
  }*/

  //Bus Company Info
  var company_name = $('#ddEmpresa.left300').text();
  var company_url = $('#ddUrl').next().text();
  var company_phone = $('#ddTelefono').text();
  var company_address = $('#ddDireccion').text();
  var company_location = $('#ddLocalidad').text();
  company_info = {name: company_name, url: company_url, phone: company_phone,
    address: company_address, location: company_location };

  //First and last stop info
  first_stop = $('.filaImpar, .filaPar').first();
  last_stop = $('.filaImpar, .filaPar').last();

  var s_town = $(first_stop).find('td').eq(0).text().trim();
  var s_stop_name = $(first_stop).find('td').eq(1).text().trim();
  var starting_stop = {town: s_town, name: s_stop_name }

  var f_town = $(last_stop).find('td').eq(0).text().trim();
  var f_stop_name = $(last_stop).find('td').eq(1).text().trim();
  var final_stop = {name: f_stop_name, town: f_town }

  //Saving trip
  db.upsert_buscompany(company_info, function(bus_company){
    db.upsert_route(route_name, route_origin, route_destiny, bus_company, function(route){
      db.upsert_stops(starting_stop, final_stop, function(stops){
        db.save_trip(route, stops, itinerary, function(trip){
          callback("trips added to route: " + trip);
        });
      });
    });
  });

}

var parse_origin_destiny_in_route = function(route_name){
  route_origin = route_name.split('-')[0].trim();

  //TODO - Fix decoding issues and save
  //route_name = 'GRANADA - JAEN'
  // route_name = 'GRANADA - JAEN (DIRECTO)'
  //route_name = 'GRANADA - CAZORLA POR JAEN'
  //route_name = "GRANADA - JAÃN SIN PASAR POR  -   PTO ARENAS, VTA MEZTAS, VTA GALLO, MOLINO ATOCHA Y NUEVO"
  //route_destiny = route_name.split('-').slice(-1)[0].trim();

  var route_destiny;
  has_sin = route_name.split('-')[1].trim().split(/\s+/).indexOf('SIN');
  if(has_sin != -1){
    route_destiny = route_name.split('-')[1].trim().split(/\s+/)[0];
  }else{
    route_destiny = route_name.split('-').slice(-1)[0].trim().split(/\([^\)]+\)/g)[0].trim().split('POR')[0].trim();
  }

  route = {origin: route_origin, destiny: route_destiny};
  return(route);
}

/* **  Querying routes ** */

/* Get two arrays of towns and store all their combinations one by one */
var itineraries_between_regions = function(reg_one_towns, reg_two_towns, callback){
  var i = 119;
  var j = 93;
  console.log("Towns in region one: " + reg_one_towns.length);
  console.log("Towns in region two: " + reg_two_towns.length);

  //Prints towns ids of a region
  /*
    for (z = 0; z < reg_two_towns.length; z++) {
      console.log(reg_two_towns[z].name);
      console.log(z);
    }
  */

  function t_db(i, j){
    if((reg_one_towns.length * reg_two_towns.length) > (i*j)){
      console.log("");
      console.log("requesting for towns: " + i + " and " + j);
      request_trip(reg_one_towns[i], reg_two_towns[j], date, function(body) {
        parse_itineraries_response(body, function(mssg){
          console.log(mssg);
          callback("request for trip completed");
          if(j == reg_two_towns.length-1){
            j = 0;
            t_db(++i, j);
          }else{
            t_db(i, ++j);
          }
        });
      });

    }else{
      console.log("completed");
      mongoose.disconnect();
    }

  }

  return t_db(i, j);
}

var get_routes_between_regions = function(region_one, region_two, callback){
  db.get_towns_for_region(region_one, function(towns_one){
    db.get_towns_for_region(region_two , function(towns_two){
      itineraries_between_regions(towns_one, towns_two, function(mssg){
        console.log(mssg);
      });
    });
  });
}

/* *** App scraper Initializer *** */

//Global Cookies not renewed by request_trip until needed
var cookies = {
                JSESSIONID: '439FA81957CA8185E5E2FE4D1273AC81',
                ARRAffinity: '82a244a919072a88ebb256e0398c414bad0241a56a01e8fc61a8ddb339bd6407'
              };

var cookie = 'JSESSIONID='+cookies.JSESSIONID+'; ARRAffinity='+cookies.ARRAffinity

var date = '18/08/2014'

//GRANADA 268 capital id: 119
//JAEN 186 capital id: 93
/**/

get_routes_between_regions('GRANADA', 'JAEN', function(mssg){
  console.log(mssg);
  mongoose.disconnect();
});
